<?php

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => 'http://10.6.20.3:8068/Exec',
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'POST',
  CURLOPT_POSTFIELDS =>'<?xml version="1.0" encoding="UTF-8"?>
<TKKPG>
    <Request>
        <Operation>Purchase</Operation>
        <OrigAmount>30</OrigAmount>
        <OrigCurrency>840</OrigCurrency>
        <Order>
            <Merchant>318111358120004</Merchant>
            <OrderID>48622</OrderID>
            <AddParams>
                <FA-DATA>Email=user@yandex.ru; Phone=22211444</FA-DATA>
                <MasterPassWalletId></MasterPassWalletId>
            </AddParams>
        </Order>
        <SessionID>20CC20F11F8868652D80AD7A03402FEC</SessionID>
        <Amount>30</Amount>
        <Currency>840</Currency>
        <PAN>5156830272561029</PAN>
        <ExpDate>0525</ExpDate>
        <CVV2>414</CVV2>
        <!-- 3-D Secure protocol approval code -->
        <CAVV>31353731313536393832300436303030303430309000010116565607005000286856508000009000</CAVV>
        <!-- TWEC indicator code -->
        <eci>00</eci>
        <DraftCaptureFlag>1</DraftCaptureFlag>
        <IP>192.167.12.10</IP>
        <isMOTO>false</isMOTO>
        <IncreaseOrderAmount>true</IncreaseOrderAmount>
        <ResponseFormat>TKKPG</ResponseFormat>
        <EncryptedPayload></EncryptedPayload>
    </Request>
</TKKPG>',
  CURLOPT_HTTPHEADER => array(
    'Content-Type: application/xml',
    'Cookie: JSESSIONID=20CC20F11F8868652D80AD7A03402FEC'
  ),
));

$response = curl_exec($curl);

curl_close($curl);
echo $response;
